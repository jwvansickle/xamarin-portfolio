﻿using System;
namespace Portfolio.Models
{
    public class Skill
    {
        public string ImageName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
