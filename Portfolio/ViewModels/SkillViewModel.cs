﻿using System;
using Portfolio.Models;

namespace Portfolio.ViewModels
{
    public class SkillViewModel
    {
        public Skill Skill { get; set; }

        public SkillViewModel(Skill skill = null)
        {
            Skill = skill;
        }
    }
}
