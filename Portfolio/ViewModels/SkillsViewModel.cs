﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Portfolio.Models;

namespace Portfolio.ViewModels
{
    public class SkillsViewModel
    {
        public ObservableCollection<Skill> Skills { get; set; }

        public SkillsViewModel()
        {
            Skills = new ObservableCollection<Skill>(skills);
        }

        List<Skill> skills = new List<Skill>
        {
            new Skill {ImageName = "azure_logo.png", Title = "Azure", Description = _azureDescription},
            new Skill {ImageName = "netcore_icon.png", Title = "Asp.NET Core 2", Description = _netcoreDescription}
        };

        readonly static string _azureDescription = "Microsoft's cloud services. I like the solutions and have used Functions, CosmosDB, Storage, and Web Apps. This website is even moved from AWS to a Web Apps solution now.";
        readonly static string _netcoreDescription = "Being able to leverage Microsoft's tools and the C# language cross-platform and hosting the web application server in an Ubuntu AWS instance is a great thing. Also, I'm a fan of the Entity Framework that is usable with .net Core.";
    }
}
