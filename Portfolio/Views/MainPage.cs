﻿using System;
using Portfolio.Views;
using Xamarin.Forms;

namespace Portfolio
{
    public class MainPage : TabbedPage
    {
        public MainPage()
        {
            Page skillsPage, itemsPage, aboutPage = null;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    skillsPage = new NavigationPage(new SkillsPage())
                    {
                        Title = "Skills",
                        Icon = "tab_feed.png"
                    };

                    //itemsPage = new NavigationPage(new ItemsPage())
                    //{
                    //    Title = "Browse"
                    //};

                    //aboutPage = new NavigationPage(new AboutPage())
                    //{
                    //    Title = "About"
                    //};
                    //itemsPage.Icon = "tab_feed.png";
                    //aboutPage.Icon = "tab_about.png";
                    break;
                default:
                    skillsPage = new SkillsPage()
                    {
                        Title = "Skills"
                    };

                    //itemsPage = new ItemsPage()
                    //{
                    //    Title = "Browse"
                    //};

                    //aboutPage = new AboutPage()
                    //{
                    //    Title = "About"
                    //};
                    break;
            }

            Children.Add(skillsPage);
            //Children.Add(itemsPage);
            //Children.Add(aboutPage);
        }

        protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();
        }
    }
}
