﻿using System;
using System.Collections.Generic;
using Portfolio.Models;
using Portfolio.ViewModels;
using Xamarin.Forms;

namespace Portfolio.Views
{
    public partial class SkillsPage : ContentPage
    {
        SkillsViewModel viewModel;

        public SkillsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new SkillsViewModel();
            Title = "Skills";
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var skill = args.SelectedItem as Skill;
            if (skill == null)
            {
                return;
            }

            await Navigation.PushAsync(new SkillDetailPage(new SkillViewModel(skill)));

            SkillsListView.SelectedItem = null;
        }
    }
}
