﻿using Portfolio.ViewModels;
using Xamarin.Forms;

namespace Portfolio.Views
{
    public partial class SkillDetailPage : ContentPage
    {
        SkillViewModel viewModel;

        public SkillDetailPage(SkillViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;

            Title = viewModel.Skill.Title;
        }
    }
}
